module.exports = {
  async up(db, client) {
    await db.collection('worklogs').updateMany({}, { $rename: { days: "logs" } })
    await db.collection('worklogs').updateMany({}, { $set: { type: "Monthly Hour Target" } })
  },

  async down(db, client) {
    await db.collection('worklogs').updateMany({}, { $rename: { logs: "days" } })
    await db.collection('worklogs').updateMany({}, { $unset: { type: "" } })
  }
};
