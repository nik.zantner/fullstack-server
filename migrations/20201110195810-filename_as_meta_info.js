// eslint-disable-next-line @typescript-eslint/no-var-requires
const mongoose = require('mongoose');
const ObjectId = mongoose.Types.ObjectId;

module.exports = {
  async up(db, client) {
    const metas = db.collection('filemetadatas').find({}, { timeout: false });

    while (await metas.hasNext()) {
      const meta = await metas.next();
      const file = await db.collection('fs.files').findOne({ _id: ObjectId(meta.fileId) });
      await db.collection('filemetadatas').findOneAndUpdate({ _id: meta.id }, { $set: { filename: file.filename }});
    }

    metas.close();
  },

  async down(db, client) {
    await db.collection('filemetadatas').updateMany({}, { $unset: { filename: "" } })
  }
};
