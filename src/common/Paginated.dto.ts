import { PaginateResult } from "mongoose";

export class PaginatedDto<T, U> {
  totalPages: number;
  totalItems: number;
  page: number;
  limit: number;
  items: T[];

  constructor(paginated: PaginateResult<U>, converter: (dao: U) => T) {
    this.items = paginated.docs.map(converter);
    this.totalPages = paginated.totalPages;
    this.totalItems = paginated.totalDocs;
    this.page = paginated.page;
    this.limit = paginated.limit;
  }
}
