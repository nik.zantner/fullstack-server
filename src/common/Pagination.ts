import { Allow } from "class-validator";

export class Pagination {
  @Allow() page: number;
  @Allow() limit: number;
  @Allow() sort: any;
  @Allow() select: any;
  @Allow() searchQuery: any;
  @Allow() searchField: any;
}
