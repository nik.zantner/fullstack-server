import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import { createTransport, Transporter } from "nodemailer";

@Injectable()
export class MailService {
  private transport: Transporter;
  private from = '"Your devs" <mail@niklaszantner.de>';

  constructor() {
    this.transport = createTransport(process.env.SMTP_TRANSPORT);
  }

  async sendMail(to: string, subject: string, html: string, from = this.from) {
    try {
      await this.transport.sendMail({
        from,
        to,
        subject,
        html,
      });
    } catch (e) {
      throw new HttpException("An error occurred while retrieving file", HttpStatus.EXPECTATION_FAILED);
    }
  }
}
