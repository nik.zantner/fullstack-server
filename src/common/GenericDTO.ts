import { ObjectId } from "mongodb";
import { Document } from "mongoose";
import { Allow } from "class-validator";

export class GenericDto {
  @Allow() id: ObjectId;
  @Allow() createdAt: Date;
  @Allow() updatedAt: Date;

  constructor(document: Document) {
    this.id = document._id;
    this.createdAt = document["createdAt"];
    this.updatedAt = document["updatedAt"];
  }
}
