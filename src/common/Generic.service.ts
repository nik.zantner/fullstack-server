import { Document, FilterQuery, PaginateModel } from "mongoose";
import { ObjectId } from "mongodb";

import { Pagination } from "./Pagination";
import { PaginatedDto } from "./Paginated.dto";
import { HttpException, HttpStatus, NotFoundException } from "@nestjs/common";

export class GenericService<DocumentClass extends Document, DTO, CreateDTO, UpdateDTO> {
  private readonly model: PaginateModel<DocumentClass>;
  private readonly toDto: (doc: DocumentClass) => DTO;

  constructor(model: PaginateModel<DocumentClass>, toDto: (doc: DocumentClass) => DTO) {
    this.model = model;
    this.toDto = toDto;
  }

  async findById(id: ObjectId): Promise<DTO> {
    const doc = await this.model.findById(id).orFail(() => {
      throw new NotFoundException(`Could not find document with id ${id}`);
    });
    return this.toDto(doc);
  }

  async paginate(pagination: Pagination, query = {}): Promise<PaginatedDto<DTO, DocumentClass>> {
    return new PaginatedDto(await this.model.paginate(query, pagination), (doc) => this.toDto(doc));
  }

  async create(create: Partial<Record<keyof DocumentClass, unknown>>, additionalProperties = {}): Promise<DTO> {
    const doc = await this.model.create({ ...create, ...additionalProperties } as DocumentClass);
    return this.toDto(doc);
  }

  async createMultiple(
    creates: Partial<Record<keyof DocumentClass, unknown>>[],
    additionalProperties = {},
  ): Promise<DTO[]> {
    const docs = await this.model.insertMany(
      creates.map((create) => ({ ...create, ...additionalProperties } as DocumentClass)),
    );
    return docs.map((doc) => this.toDto(doc));
  }

  async delete(id: ObjectId): Promise<any> {
    return this.model.findByIdAndDelete(id);
  }

  async deleteMultiple(ids: ObjectId[]): Promise<any> {
    // @ts-ignore
    return this.model.deleteMany({ _id: { $in: ids } });
  }

  async update(id: ObjectId, update: UpdateDTO): Promise<DTO> {
    // @ts-ignore
    const doc = await this.model.findByIdAndUpdate(id, update, { new: true });
    return this.toDto(doc);
  }

  async updateMultiple(id: ObjectId, updates: UpdateDTO[]): Promise<DTO> {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    const docs = await this.model.updateMany({ _id: { $in: updates.map(({ id }) => id) } }, updates, { new: true });
    // @ts-ignore
    return docs.map((doc) => this.toDto(doc));
  }

  async guardAccessMultiple(ids: ObjectId[], ownerId: ObjectId, ownerProperty = "ownerId"): Promise<void> {
    // @ts-ignore
    const docs = await this.model.find({ _id: { $in: ids } });

    if (docs.some((cur) => !new ObjectId(cur[ownerProperty]).equals(ownerId))) {
      throw new HttpException("Forbidden", HttpStatus.FORBIDDEN);
    }
  }

  async guardAccess(id: ObjectId, ownerId: ObjectId, ownerProperty = "ownerId"): Promise<void> {
    const doc = await this.model.findById(id);

    if (!new ObjectId(doc[ownerProperty]).equals(ownerId)) {
      throw new HttpException("Forbidden", HttpStatus.FORBIDDEN);
    }
  }
}
