import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate-v2";

@Schema({ timestamps: true })
export class Token extends Document {
  @Prop({ required: true })
  entityId: string;
}

export const TokenSchema = SchemaFactory.createForClass(Token);

// @ts-ignore
TokenSchema.plugin(mongoosePaginate);
