import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";

import { TokenSchema, Token } from "./Token.schema";

@Module({
  imports: [MongooseModule.forFeature([{ name: Token.name, schema: TokenSchema }])],
  providers: [],
  controllers: [],
  exports: [MongooseModule],
})
export class TokenModule {}
