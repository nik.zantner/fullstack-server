import { ObjectId } from "mongodb";
import { ApiTags } from "@nestjs/swagger";
import {
  Controller,
  Get,
  Delete,
  Request,
  Param,
  Query,
  Body,
  Post,
  Patch,
  HttpException,
  HttpStatus,
} from "@nestjs/common";
import { UserService } from "./User.service";
import { UserDto, CreateUserDto, UpdateUserDto, PasswordUpdate } from "./User.dto";
import { RequiredRoles } from "../auth/guards/Roles.decorator";
import { Roles } from "../auth/roles/Roles.enum";
import { Pagination } from "../common/Pagination";
import { PaginatedDto } from "../common/Paginated.dto";
import { User } from "./User.schema";
import { RequestWithUser } from "../auth/strategies/JWT.strategy";

@ApiTags("users")
@Controller("users")
export class UserController {
  constructor(private userService: UserService) {}

  @Get("/me")
  async getMe(@Request() req: RequestWithUser): Promise<UserDto> {
    return this.userService.findById(new ObjectId(req.user.userId));
  }

  @RequiredRoles(Roles.ADMIN)
  @Get("")
  async findAll(@Query() query: Pagination): Promise<PaginatedDto<UserDto, User>> {
    return this.userService.paginate(query);
  }

  @RequiredRoles(Roles.ADMIN)
  @Post()
  async create(@Body() createUserDto: CreateUserDto): Promise<UserDto> {
    return this.userService.createWithEmailNotification(createUserDto);
  }

  @RequiredRoles(Roles.USER)
  @Patch("/:id")
  async update(
    @Request() req: RequestWithUser,
    @Param("id") id: string,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<UserDto> {
    if (!req.user.roles.includes(Roles.ADMIN) && !new ObjectId(req.user.userId).equals(id)) {
      throw new HttpException("Forbidden", HttpStatus.FORBIDDEN);
    }

    return this.userService.update(new ObjectId(id), updateUserDto);
  }

  @RequiredRoles(Roles.USER)
  @Patch("/:id/change-password")
  async updatePassword(
    @Request() req: RequestWithUser,
    @Param("id") id: string,
    @Body() passwordUpdate: PasswordUpdate,
  ): Promise<UserDto> {
    if (new ObjectId(req.user.userId).equals(id)) {
      throw new HttpException("Forbidden", HttpStatus.FORBIDDEN);
    }

    return this.userService.updatePassword(id, passwordUpdate.oldPassword, passwordUpdate.newPassword);
  }

  @RequiredRoles(Roles.ADMIN)
  @Delete("/:id")
  async delete(@Param("id") id: string): Promise<any> {
    return this.userService.delete(new ObjectId(id));
  }
}
