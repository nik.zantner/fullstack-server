import { Roles } from "../auth/roles/Roles.enum";
import { User } from "./User.schema";
import { Allow } from "class-validator";
import { GenericDto } from "../common/GenericDTO";

export class UserDto extends GenericDto {
  @Allow() email: string;
  @Allow() roles: Roles[];
  @Allow() firstName: string;
  @Allow() lastName: string;
  @Allow() isActivated: boolean;

  constructor(user: User) {
    super(user);

    this.firstName = user.firstName;
    this.lastName = user.lastName;
    this.roles = user.roles;
    this.email = user.email;
    this.isActivated = user.isActivated;
    return this;
  }
}

export class CreateUserDto {
  @Allow() email: string;
  @Allow() roles: Roles[];
  @Allow() firstName: string;
  @Allow() lastName: string;
}

export class UpdateUserDto {
  @Allow() email: string;
  @Allow() roles: Roles[];
  @Allow() firstName: string;
  @Allow() lastName: string;
  @Allow() isActivated: boolean;
}

export class PasswordUpdate {
  @Allow() oldPassword: string;
  @Allow() newPassword: string;
}
