import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import * as mongoosePaginate from "mongoose-paginate-v2";
import { Roles } from "../auth/roles/Roles.enum";

@Schema({ timestamps: true })
export class User extends Document {
  @Prop()
  password: string;

  @Prop({ required: true })
  email: string;

  @Prop()
  firstName: string;

  @Prop()
  lastName: string;

  @Prop()
  isActivated: boolean;

  @Prop()
  roles: Roles[];

  @Prop()
  cryptoApiKey: string;
}

export const UserSchema = SchemaFactory.createForClass(User);

// @ts-ignore
UserSchema.plugin(mongoosePaginate);
