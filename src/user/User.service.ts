import { Injectable, HttpException, HttpStatus } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { PaginateModel, Document } from "mongoose";

import { User } from "./User.schema";
import { comparePassword, hashPassword } from "../auth/Auth.utils";
import { Roles } from "../auth/roles/Roles.enum";
import { UserDto, CreateUserDto, UpdateUserDto } from "./User.dto";
import { Token } from "../token/Token.schema";
import { GenericService } from "../common/Generic.service";
import { MailService } from "../common/Mail/Mail.service";

@Injectable()
export class UserService extends GenericService<User, UserDto, CreateUserDto, UpdateUserDto> {
  constructor(
    @InjectModel(User.name) private userModel: PaginateModel<User>,
    @InjectModel(Token.name) private tokenModel: PaginateModel<Token>,
    private mailService: MailService,
  ) {
    super(userModel, (user: User) => new UserDto(user));
  }

  async onModuleInit(): Promise<void> {
    const admin = await this.userModel.findOne({ email: process.env.ADMIN_EMAIL });

    if (!admin) {
      await this.userModel.create({
        email: process.env.ADMIN_EMAIL,
        password: await hashPassword(process.env.ADMIN_PASSWORD),
        firstName: "Super",
        lastName: "Admin",
        roles: [Roles.ADMIN],
        isActivated: true,
        notes: [],
        cryptoCurrencies: [],
        cryptoApiKey: null,
      });
    }
  }

  async createWithEmailNotification(userToCreate: CreateUserDto): Promise<UserDto> {
    const user = await this.userModel.create({
      ...userToCreate,
      password: null,
      isActivated: false,
      notes: [],
      cryptoCurrencies: [],
      cryptoApiKey: null,
    });

    const token =
      (await this.tokenModel.findOne({ entityId: user.id })) || (await this.tokenModel.create({ entityId: user.id }));
    const link = `${process.env.CLIENT_URL}/verify?token=${token.id}&userId=${token.entityId}`;
    try {
      await this.mailService.sendMail(
        user.email,
        "Signup Link",
        "<p>Open following link to set a password and activate your account:</p>" + `<a href="${link}">${link}</a>`,
      );
    } catch (e) {
      throw new HttpException(`Could not send email: ${e}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return new UserDto(user);
  }

  async updatePassword(id: string, oldPassword: string, newPassword: string): Promise<UserDto> {
    const user = await this.userModel.findById(id);

    const isValid = await comparePassword(oldPassword, user.password);
    if (isValid) {
      return new UserDto(
        await this.userModel.findByIdAndUpdate(id, { password: await hashPassword(newPassword) }, { new: true }),
      );
    }

    throw new HttpException("Forbidden", HttpStatus.FORBIDDEN);
  }
}
