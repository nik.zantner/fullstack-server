import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";

import { UserService } from "./User.service";
import { UserSchema, User } from "./User.schema";
import { UserController } from "./User.controller";
import { TokenModule } from "../token/Token.module";
import { MailModule } from "../common/Mail/Mail.module";

@Module({
  imports: [MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]), TokenModule, MailModule],
  providers: [UserService],
  controllers: [UserController],
  exports: [UserService, MongooseModule],
})
export class UserModule {}
