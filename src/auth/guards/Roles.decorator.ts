import { SetMetadata, CustomDecorator } from "@nestjs/common";
import { Roles } from "../roles/Roles.enum";

export const RequiredRoles = (...roles: Roles[]): CustomDecorator => SetMetadata("roles", roles);
