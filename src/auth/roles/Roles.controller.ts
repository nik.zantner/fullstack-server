import { ApiTags } from "@nestjs/swagger";
import { Controller, Get } from "@nestjs/common";
import { RequiredRoles } from "../guards/Roles.decorator";
import { Roles } from "./Roles.enum";

@ApiTags("roles")
@Controller("roles")
export class RolesController {
  @RequiredRoles(Roles.ADMIN)
  @Get()
  async findAll(): Promise<string[]> {
    return Object.keys(Roles);
  }
}
