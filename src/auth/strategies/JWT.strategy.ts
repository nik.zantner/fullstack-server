import { ObjectId } from "mongodb";
import { Injectable } from "@nestjs/common";
import { PassportStrategy } from "@nestjs/passport";
import { ExtractJwt, Strategy } from "passport-jwt";
import { Request } from "express";
import { Roles } from "../roles/Roles.enum";

export type AuthUser = {
  userId: ObjectId;
  email: string;
  roles: Roles[];
};

@Injectable()
export class JWTStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: process.env.JWT_SECRET,
    });
  }

  async validate(payload: any): Promise<AuthUser> {
    return { userId: payload.sub, email: payload.email, roles: payload.roles };
  }
}

export type RequestWithUser = Request & { user: AuthUser };
