import { ObjectId } from "mongodb";
import {
  ConflictException,
  Injectable,
  UnprocessableEntityException,
  HttpException,
  HttpStatus,
  NotFoundException,
} from "@nestjs/common";
import { JwtService } from "@nestjs/jwt";
import { InjectModel } from "@nestjs/mongoose";
import { Model, PaginateModel } from "mongoose";

import { comparePassword, hashPassword } from "./Auth.utils";
import { AuthCredentialsDto } from "./AuthCredentials.dto";
import { User } from "../user/User.schema";
import { Roles } from "./roles/Roles.enum";
import { Token } from "../token/Token.schema";
import { AuthUser } from "./strategies/JWT.strategy";
import { MailService } from "../common/Mail/Mail.service";

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(User.name) private userModel: Model<User>,
    @InjectModel(Token.name) private tokenModel: PaginateModel<Token>,
    private jwtService: JwtService,
    private mailService: MailService,
  ) {}

  async signUp(authCredentialsDto: AuthCredentialsDto): Promise<void> {
    const { email, password } = authCredentialsDto;

    const hashedPassword = await hashPassword(password);

    const user = new this.userModel({ email, password: hashedPassword });

    try {
      await user.save();
    } catch (error) {
      if (error.code === 11000) {
        throw new ConflictException("User already exists");
      }
      throw error;
    }
  }

  async login(email: string, userId: ObjectId, roles: Roles[]): Promise<Record<string, any>> {
    return {
      accessToken: this.jwtService.sign({ email: email, sub: userId, roles: roles }),
      expiresIn: process.env.JWT_TIMEOUT_IN_SECONDS,
    };
  }

  async validateUser(email: string, pass: string): Promise<User> {
    const user = await this.userModel.findOne({ email });

    if (!user) {
      return null;
    }

    if (!user.isActivated) {
      throw new UnprocessableEntityException("Your user is not yet activated, check your email.");
    }

    const valid = await comparePassword(pass, user.password);

    if (valid) {
      return user;
    }

    return null;
  }

  async verify(password: string, tokenId: string, userId: string): Promise<void> {
    const token = await this.tokenModel.findById(tokenId);

    if (token.entityId === userId) {
      await this.userModel.findByIdAndUpdate(
        userId,
        { password: await hashPassword(password), isActivated: true },
        { new: true },
      );
      await token.deleteOne();
      return;
    }

    throw new HttpException("Forbidden", HttpStatus.FORBIDDEN);
  }

  async resetPassword(email: string): Promise<void> {
    const user = await this.userModel.findOne({ email });

    if (!user) {
      throw new NotFoundException(`The user with the email ${email} does not exist`);
    }

    const token =
      (await this.tokenModel.findOne({ entityId: user.id })) || (await this.tokenModel.create({ entityId: user.id }));
    const link = `${process.env.CLIENT_URL}/verify?token=${token.id}&userId=${token.entityId}`;
    try {
      await this.mailService.sendMail(
        user.email,
        "Password Reset Link",
        "<p>Open following link to set a new password:</p>" + `<a href="${link}">${link}</a>`,
      );
    } catch (e) {
      throw new HttpException(`Could not send email: ${e}`, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  refresh(user: AuthUser) {
    return {
      accessToken: this.jwtService.sign({ email: user.email, sub: user.userId, roles: user.roles }),
      expiresIn: process.env.JWT_TIMEOUT_IN_SECONDS,
    };
  }
}
