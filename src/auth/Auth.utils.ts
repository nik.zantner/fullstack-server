import * as bcrypt from "bcrypt";

export const hashPassword = async (password: string): Promise<string> => bcrypt.hash(password, 10);

export const comparePassword = async (password: string, hash: string): Promise<any> => bcrypt.compare(password, hash);
