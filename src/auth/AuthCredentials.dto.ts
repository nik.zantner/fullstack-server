import { Allow } from "class-validator";

export class AuthCredentialsDto {
  @Allow() email: string;
  @Allow() password: string;
  @Allow() roles: [string];
}
