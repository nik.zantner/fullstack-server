import { ApiTags } from "@nestjs/swagger";
import { Controller, Post, Request, UseGuards, Patch, Body } from "@nestjs/common";

import { AuthService } from "./Auth.service";
import { LocalAuthGuard } from "./guards/Local.guard";
import { Public } from "./guards/Public.decorator";
import { RequestWithUser } from "./strategies/JWT.strategy";
import { RequiredRoles } from "./guards/Roles.decorator";
import { Roles } from "./roles/Roles.enum";

@ApiTags("auth")
@Controller("auth")
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Public()
  @Post("/login")
  async login(@Request() { user }: any): Promise<any> {
    return this.authService.login(user.email, user.id, user.roles);
  }

  @Post("/refresh")
  @RequiredRoles(Roles.USER)
  async refresh(@Request() req: RequestWithUser): Promise<any> {
    return this.authService.refresh(req.user);
  }

  @Public()
  @Patch("/verify")
  async verify(
    @Body() { password, token, userId }: { password: string; token: string; userId: string },
  ): Promise<void> {
    return this.authService.verify(password, token, userId);
  }

  @Public()
  @Post("/reset-password")
  async resetPassword(@Body() { email }: { email: string }): Promise<void> {
    return this.authService.resetPassword(email);
  }
}
