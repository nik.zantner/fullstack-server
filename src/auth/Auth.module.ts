import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { JwtModule } from "@nestjs/jwt";
import { MongooseModule } from "@nestjs/mongoose";
import { PassportModule } from "@nestjs/passport";

import { AuthController } from "./Auth.controller";
import { AuthService } from "./Auth.service";
import { UserSchema, User } from "../user/User.schema";
import { JWTStrategy } from "./strategies/JWT.strategy";
import { LocalStrategy } from "./strategies/Local.strategy";
import { RolesController } from "./roles/Roles.controller";
import { TokenModule } from "../token/Token.module";
import { MailModule } from "../common/Mail/Mail.module";

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
    PassportModule,
    TokenModule,
    MailModule,
    JwtModule.register({
      secret: process.env.JWT_SECRET,
      signOptions: { expiresIn: `${process.env.JWT_TIMEOUT_IN_SECONDS}s` },
    }),
  ],
  providers: [AuthService, LocalStrategy, JWTStrategy],
  controllers: [AuthController, RolesController],
  exports: [AuthService],
})
export class AuthModule {}
