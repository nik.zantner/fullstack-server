import { NestFactory, Reflector } from "@nestjs/core";
import { ValidationPipe } from "@nestjs/common";
import * as compression from "compression";
import helmet from 'helmet';
import { SwaggerModule, DocumentBuilder } from "@nestjs/swagger";
import { AppModule } from "./App.module";
import { RolesGuard } from "./auth/guards/Role.guard";
import { JwtAuthGuard } from "./auth/guards/JWT.guard";

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const cors = {
    origin: process.env.CORS_ORIGIN,
    methods: "POST,GET,PUT,PATCH,OPTIONS,DELETE",
    allowedHeaders: "Timezone-Offset,Origin,X-Requested-With,Content-Type,Accept,Authorization",
  };

  app.setGlobalPrefix("api");
  app.enableCors(cors);
  app.use(helmet());
  app.use(compression());

  const reflector = app.get(Reflector);
  app.useGlobalGuards(new JwtAuthGuard(reflector));
  app.useGlobalGuards(new RolesGuard(reflector));
  app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));

  const options = new DocumentBuilder()
    .setTitle("API Docs")
    .setDescription("The API documentation")
    .setVersion("1.0")
    .addTag("users")
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup("docs", app, document);

  await app.listen(process.env.PORT);
}

bootstrap();
