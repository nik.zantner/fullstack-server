import { Injectable } from "@nestjs/common";
import { MulterModuleOptions, MulterOptionsFactory } from "@nestjs/platform-express";
import { GridFsStorage } from "multer-gridfs-storage";
import { ObjectId } from "mongodb";

@Injectable()
export class GridFsMulterConfigService implements MulterOptionsFactory {
  private readonly gridFsStorage: any;

  constructor() {
    this.gridFsStorage = new GridFsStorage({
      url: process.env.MONGO_URL,
      file: async (req, file) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        return { filename: file.originalname.trim(), metadata: { ownerId: new ObjectId(req.user.userId) } };
      },
    });
  }

  createMulterOptions(): MulterModuleOptions {
    return {
      storage: this.gridFsStorage,
    };
  }
}
