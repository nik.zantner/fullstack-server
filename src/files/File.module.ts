import { Module } from "@nestjs/common";
import { MulterModule } from "@nestjs/platform-express";
import { FileController } from "./File.controller";
import { GridFsMulterConfigService } from "./MulterConfig.service";
import { FileService } from "./File.service";
import { MongooseModule } from "@nestjs/mongoose";
import { FileMetaData, FileMetaDataSchema } from "./File.schema";

@Module({
  imports: [
    MongooseModule.forFeature([{ name: FileMetaData.name, schema: FileMetaDataSchema }]),
    MulterModule.registerAsync({
      useClass: GridFsMulterConfigService,
    }),
  ],
  controllers: [FileController],
  providers: [GridFsMulterConfigService, FileService],
})
export class FileModule {}
