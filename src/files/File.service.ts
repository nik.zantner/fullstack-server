import { Injectable, HttpException, HttpStatus } from "@nestjs/common";
import { InjectConnection, InjectModel } from "@nestjs/mongoose";
import { Connection, Model } from "mongoose";
import { GridFSBucketReadStream } from "mongodb";
import { ObjectId } from "mongodb";
import { createModel } from "mongoose-gridfs";

import { CreateFileMetaDataDto, FileInfoDto, UpdateFileMetaDataDto } from "./File.dto";
import { FileMetaData } from "./File.schema";

@Injectable()
export class FileService {
  private fileModel: any;

  constructor(
    @InjectModel(FileMetaData.name) private fileMetaDataModel: Model<FileMetaData>,
    @InjectConnection() private readonly connection: Connection,
  ) {
    this.fileModel = createModel({ bucketName: "fs", connection: this.connection });
  }

  async readStream(id: string): Promise<GridFSBucketReadStream> {
    return await this.fileModel.read({ _id: new ObjectId(id) });
  }

  async findAll(ownerId: ObjectId): Promise<FileInfoDto[]> {
    const files = await this.fileModel.find({ "metadata.ownerId": ownerId });
    const filesWithMetaData = [];

    for (const file of files) {
      const metaData = await this.fileMetaDataModel.findOne({ fileId: file._id });
      filesWithMetaData.push(new FileInfoDto(file, metaData));
    }

    return filesWithMetaData.sort((a, b) => b.metadata.updatedAt.getTime() - a.metadata.updatedAt.getTime());
  }

  async findInfo(fileId: string, ownerId: ObjectId): Promise<FileInfoDto> {
    const doc = await this.fileModel.findById(fileId).catch(() => {
      throw new HttpException("File not found", HttpStatus.NOT_FOUND);
    });

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    if (!doc.metadata.ownerId || !new ObjectId(doc.metadata.ownerId).equals(ownerId)) {
      throw new HttpException("Forbidden", HttpStatus.FORBIDDEN);
    }

    const metaData = await this.fileMetaDataModel.findOne({ fileId: new ObjectId(fileId) });
    return new FileInfoDto(doc, metaData);
  }

  async updateFileInfo(fileId: string, update: UpdateFileMetaDataDto, ownerId: ObjectId): Promise<FileInfoDto> {
    const file = await this.fileModel.findById(fileId);

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    if (!file.metadata.ownerId || !new ObjectId(file.metadata.ownerId).equals(ownerId)) {
      throw new HttpException("Forbidden", HttpStatus.FORBIDDEN);
    }

    const metaData = await this.fileMetaDataModel.findOneAndUpdate({ fileId: new ObjectId(fileId) }, { ...update });
    return new FileInfoDto(file, metaData);
  }

  async createFileMetaData(fileId: ObjectId, create: CreateFileMetaDataDto): Promise<FileMetaData> {
    return this.fileMetaDataModel.create({ fileId, ...create, tags: [] });
  }

  async deleteFile(fileId: string, ownerId: ObjectId): Promise<void> {
    const doc = await this.fileModel.findById(fileId);

    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    if (!doc.metadata.ownerId || !new ObjectId(doc.metadata.ownerId).equals(ownerId)) {
      throw new HttpException("Forbidden", HttpStatus.FORBIDDEN);
    }

    await this.unlink(doc._id);
    await this.fileMetaDataModel.deleteOne({ fileId: new ObjectId(fileId) });
  }

  private unlink(id: ObjectId): Promise<void> {
    return new Promise((resolve, reject) => {
      // @ts-ignore
      this.fileModel.unlink(
        {
          _id: id,
        },
        (error: any) => {
          if (!error) {
            resolve();
          } else {
            reject();
          }
        },
      );
    });
  }
}
