import { Prop, raw, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import { ObjectId } from "mongodb";
import { Tag } from "../tags/Tag.schema";

@Schema({ timestamps: true })
export class FileMetaData extends Document {
  @Prop()
  fileId: ObjectId;

  @Prop()
  currentPage: number;

  @Prop()
  scale: number;

  @Prop()
  tags: Tag[];

  @Prop()
  filename: string;
}

export const FileMetaDataSchema = SchemaFactory.createForClass(FileMetaData);
