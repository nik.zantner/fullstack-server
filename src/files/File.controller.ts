import {
  Post,
  Get,
  Param,
  Res,
  Controller,
  UseInterceptors,
  UploadedFiles,
  HttpException,
  HttpStatus,
  Patch,
  Body,
  Request,
  Delete,
} from "@nestjs/common";
import { ApiConsumes, ApiTags } from "@nestjs/swagger";
import { FilesInterceptor } from "@nestjs/platform-express";
import { ObjectId } from "mongodb";

import { FileService } from "./File.service";
import { FileInfoDto, UpdateFileMetaDataDto } from "./File.dto";
import { RequiredRoles } from "../auth/guards/Roles.decorator";
import { Roles } from "../auth/roles/Roles.enum";
import { RequestWithUser } from "../auth/strategies/JWT.strategy";

@ApiTags("files")
@Controller("files")
export class FileController {
  constructor(private filesService: FileService) {}
  @Post("")
  @ApiConsumes("multipart/form-data")
  @UseInterceptors(FilesInterceptor("files"))
  @RequiredRoles(Roles.USER)
  async upload(@UploadedFiles() files, @Request() req: RequestWithUser): Promise<FileInfoDto[]> {
    const filesWithMetaData = [];

    for (const file of files) {
      const metaData = await this.filesService.createFileMetaData(file.id, {
        currentPage: 1,
        scale: 1,
        ownerId: new ObjectId(req.user.userId),
        filename: file.filename,
      });
      filesWithMetaData.push(new FileInfoDto(file, metaData));
    }

    return filesWithMetaData;
  }

  @Get("")
  @RequiredRoles(Roles.USER)
  findAll(@Request() req: RequestWithUser): Promise<FileInfoDto[]> {
    return this.filesService.findAll(new ObjectId(req.user.userId));
  }

  @RequiredRoles(Roles.USER)
  @Get(":id/info")
  async getFileInfo(@Param("id") id: string, @Request() req: RequestWithUser): Promise<FileInfoDto> {
    const file = await this.filesService.findInfo(id, new ObjectId(req.user.userId));
    const filestream = await this.filesService.readStream(id);
    if (!filestream) {
      throw new HttpException("An error occurred while retrieving file info", HttpStatus.EXPECTATION_FAILED);
    }
    return file;
  }

  @RequiredRoles(Roles.USER)
  @Patch(":id/info")
  async updateFileInfo(
    @Body() update: UpdateFileMetaDataDto,
    @Param("id") id: string,
    @Request() req: RequestWithUser,
  ): Promise<FileInfoDto> {
    return this.filesService.updateFileInfo(id, update, new ObjectId(req.user.userId));
  }

  @RequiredRoles(Roles.USER)
  @Get(":id")
  async getFile(@Param("id") id: string, @Res() res, @Request() req: RequestWithUser) {
    const file = await this.filesService.findInfo(id, new ObjectId(req.user.userId));
    const filestream = await this.filesService.readStream(id);
    if (!filestream) {
      throw new HttpException("An error occurred while retrieving file", HttpStatus.EXPECTATION_FAILED);
    }
    res.header("Content-Type", file.contentType);
    return filestream.pipe(res);
  }

  @RequiredRoles(Roles.USER)
  @Delete(":id")
  async deleteFile(@Param("id") id: string, @Request() req: RequestWithUser): Promise<void> {
    await this.filesService.deleteFile(id, new ObjectId(req.user.userId));
  }
}
