import { Allow } from "class-validator";
import { ObjectId } from "mongodb";

import { GenericDto } from "../common/GenericDTO";
import { FileMetaData } from "./File.schema";
import { Tag } from "../tags/Tag.schema";

export class FileInfoDto extends GenericDto {
  @Allow() length: number;
  @Allow() chunkSize: number;
  @Allow() filename: string;
  @Allow() md5: string;
  @Allow() contentType: string;
  @Allow() uploadDate: Date;
  @Allow() metadata: Record<string, any>;

  constructor(doc: any, metadata: FileMetaData) {
    super(doc);

    this.filename = doc.filename;
    this.length = doc.length;
    this.chunkSize = doc.chunkSize;
    this.md5 = doc.md5;
    this.contentType = doc.contentType;
    this.metadata = metadata;
    this.uploadDate = doc["uploadDate"];
    return this;
  }
}

export class UpdateFileMetaDataDto {
  @Allow() currentPage: number;
  @Allow() scale: number;
  @Allow() tags: Tag[];
  @Allow() filename: string;
}

export class CreateFileMetaDataDto {
  @Allow() currentPage: number;
  @Allow() scale: number;
  @Allow() ownerId: ObjectId;
  @Allow() filename: string;
}
