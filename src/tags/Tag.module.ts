import { Module } from "@nestjs/common";
import { TagController } from "./Tag.controller";
import { TagService } from "./Tag.service";
import { MongooseModule } from "@nestjs/mongoose";
import { Tag, TagSchema } from "./Tag.schema";

@Module({
  imports: [MongooseModule.forFeature([{ name: Tag.name, schema: TagSchema }])],
  controllers: [TagController],
  providers: [TagService],
})
export class TagModule {}
