import { Allow } from "class-validator";
import { ObjectId } from "mongodb";

import { GenericDto } from "../common/GenericDTO";
import { Tag } from "./Tag.schema";

export class TagDto extends GenericDto {
  @Allow() label: string;
  @Allow() color: string;

  constructor(tag: Tag) {
    super(tag);

    this.label = tag.label;
    this.color = tag.color;

    return this;
  }
}

export class UpdateTagDto {
  @Allow() label: string;
  @Allow() color: string;
}
