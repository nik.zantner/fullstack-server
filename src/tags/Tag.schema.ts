import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";
import { ObjectId } from "mongodb";

@Schema({ timestamps: true })
export class Tag extends Document {
  @Prop()
  label: string;

  @Prop()
  color: string;

  @Prop({ ref: "User", required: true })
  ownerId: ObjectId;
}

export const TagSchema = SchemaFactory.createForClass(Tag);
