import { Post, Get, Param, Controller, Patch, Body, Request, Delete } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { ObjectId } from "mongodb";

import { TagService } from "./Tag.service";
import { TagDto, UpdateTagDto } from "./Tag.dto";
import { RequiredRoles } from "../auth/guards/Roles.decorator";
import { Roles } from "../auth/roles/Roles.enum";
import { RequestWithUser } from "../auth/strategies/JWT.strategy";

@ApiTags("tags")
@Controller("tags")
export class TagController {
  constructor(private tagService: TagService) {}

  @RequiredRoles(Roles.USER)
  @Get("")
  findAll(@Request() req: RequestWithUser): Promise<TagDto[]> {
    return this.tagService.findAll(new ObjectId(req.user.userId));
  }

  @RequiredRoles(Roles.USER)
  @Post("")
  async create(@Request() req: RequestWithUser, @Body() create: UpdateTagDto): Promise<TagDto> {
    return this.tagService.create(create, { ownerId: new ObjectId(req.user.userId) });
  }

  @RequiredRoles(Roles.USER)
  @Delete(":id")
  async delete(@Param("id") id: ObjectId, @Request() req: RequestWithUser): Promise<void> {
    await this.tagService.deleteGuarded(id, new ObjectId(req.user.userId));
  }

  @RequiredRoles(Roles.USER)
  @Patch(":id")
  async update(
    @Param("id") id: ObjectId,
    @Request() req: RequestWithUser,
    @Body() update: UpdateTagDto,
  ): Promise<void> {
    await this.tagService.updateGuarded(id, new ObjectId(req.user.userId), update);
  }
}
