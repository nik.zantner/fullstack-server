import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { PaginateModel } from "mongoose";
import { ObjectId } from "mongodb";

import { Tag } from "./Tag.schema";
import { TagDto, UpdateTagDto } from "./Tag.dto";
import { GenericService } from "../common/Generic.service";

@Injectable()
export class TagService extends GenericService<Tag, TagDto, UpdateTagDto, UpdateTagDto> {
  constructor(@InjectModel(Tag.name) private tagModel: PaginateModel<Tag>) {
    super(tagModel, (tag: Tag) => new TagDto(tag));
  }

  async findAll(ownerId: ObjectId): Promise<TagDto[]> {
    const tags = await this.tagModel.find({ ownerId: ownerId });
    return tags.map((tag) => new TagDto(tag));
  }

  async updateGuarded(id: ObjectId, ownerId: ObjectId, update: UpdateTagDto): Promise<UpdateTagDto> {
    await this.guardAccess(id, ownerId);
    return this.update(id, update);
  }

  async deleteGuarded(id: ObjectId, ownerId: ObjectId): Promise<any> {
    await this.guardAccess(id, ownerId);
    return this.delete(id);
  }
}
