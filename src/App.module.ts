import { Module } from "@nestjs/common";
import { ConfigModule } from "@nestjs/config";
import { MongooseModule } from "@nestjs/mongoose";

import { AuthModule } from "./auth/Auth.module";
import { UserModule } from "./user/User.module";
import { TokenModule } from "./token/Token.module";
import { FileModule } from "./files/File.module";
import { TagModule } from "./tags/Tag.module";

@Module({
  imports: [
    ConfigModule.forRoot(),
    MongooseModule.forRoot(process.env.MONGO_URL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    }),
    AuthModule,
    UserModule,
    TokenModule,
    FileModule,
    TagModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
