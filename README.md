## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Deploy recommendations and dependencies
pm2
nginx
micro
gitlab-runner
mongodb
npm
node
ufw

... and some config:
sudo setfacl -m user:gitlab-runner:rwx client
